import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class SampleTest {

	@Test
	public void simpleAssertionTest() {
		String sentence = "COOL";
		System.out.println(sentence);

		assertTrue(sentence.equalsIgnoreCase("cool"));
	}
}
